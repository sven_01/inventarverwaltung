<?php

return [
    "model" => "Aufträge",
    "add" => "Auftrag hinzufügen",
    "create" => "Auftrag erstellen",
    "edit" => "Auftrag bearbeiten",
    "list" => "Aufträge Liste",
    "customer" => "Kunde",
    "title" => "Bezeichnung",
    "product" => "Produkt",
    "actual" => "Geliefert",
    "target" => "Zu liefern",
    'missing_products' => "Fehlende Produkte (Service-Box)",
    'missing_products_full' => "Folgende Produkte fehlen in Ihrer Service-Box",
    "quantity_missing" => "Fehlende Menge",
    'order_nr' => 'Nummer',
    'add_missing_products_to_box' => 'Fehlende Produkte hinzufügen',
];
