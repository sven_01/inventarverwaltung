<?php

return [
    "model" => "Produkte",
    "add" => "Produkt hinzufügen",
    "edit" => "Produkt bearbeiten",
    "create" => "Produkt erstellen",
    "list" => "Produkt Liste",
    'id'                       => 'ID',
    'id_helper'                => '',
    'description' => 'Beschreibung',
    'description_helper' => '',
    'title' => 'Bezeichnung',
    'title_helper'              => '',
    'quantity' => 'Menge',
    'quantity_helper' => '',
    'quantity_alert' => 'Menge (Benachrichtigung)',
    'quantity_alert_helper' => '',
    'quantity_add' => 'Hinzufügen',
    'quantity_remove' => 'Entfernen',
    'number' => 'Artikelnummer',
    'number_helper' => '',
    'no_products' => 'Keine Produkte vorhanden',
];
