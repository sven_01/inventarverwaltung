<?php

return [
    "model" => "Box-Vorlagen",
    "add" => "Vorlage hinzufügen",
    "create" => "Vorlage erstellen",
    "edit" => "Vorlage bearbeiten",
    "list" => "Vorlagen Liste",
    'title' => 'Bezeichnung',
    'title_helper' => '',
    'products' => 'Produkte',
    'products_helper' => '',
    'product' => 'Produkt',
    'target' => 'Menge (Soll)',
];
