<?php

return [
    "model" => "Rollen",
    "add" => "Rolle hinzufügen",
    "edit" => "Rolle bearbeiten",
    "create" => "Rolle erstellen",
    "list" => "Rollen Liste",
    'id'                       => 'ID',
    'id_helper'                => '',
    'name'                     => 'Name',
    'name_helper'              => '',
    'created_at'               => 'Erstellt',
    'created_at_helper'        => '',
    'updated_at'               => 'Aktualisiert',
    'updated_at_helper'        => '',
    'deleted_at'               => 'Gelöscht',
    'deleted_at_helper'        => '',
    'permissions' => 'Rechte',
    'permissions_helper' => '',
    'roles_helper' => '',
];
