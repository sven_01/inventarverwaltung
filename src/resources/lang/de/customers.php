<?php

return [
    "model" => "Kunden",
    "add" => "Kunden hinzufügen",
    "create" => "Kunden erstellen",
    "edit" => "Kunden bearbeiten",
    "list" => "Kunden Liste",
    "company" => "Firma",
    "first_name" => "Vorname",
    "last_name" => "Nachname",
    "street" => "Straße",
    "zip" => "PLZ",
    "city" => "Stadt",
    "email" => "E-Mail",
    "phone" => "Telefonnummer",
    "website" => "Website",
];
