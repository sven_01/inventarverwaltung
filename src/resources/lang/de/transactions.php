<?php

return [
    "model" => "Transaktionen",
    "add" => "Transaktion hinzufügen",
    "edit" => "Transaktion bearbeiten",
    "create" => "Transaktion erstellen",
    "list" => "Transaktionen Liste",
    'id'                       => 'ID',
    'id_helper'                => '',
    'description'              => 'Bemerkung',
    'quantity'                 => 'Menge',
    'empty'                 => 'Keine Transaktionen vorhanden.',
    'created_at'               => 'Datum',
    'created_at_helper'        => '',
    'updated_at'               => 'Aktualisiert',
    'updated_at_helper'        => '',
    'deleted_at'               => 'Gelöscht',
    'deleted_at_helper'        => '',
    'descriptions' => [
        'service_box' => 'Service-Box.',
        'order' => 'Auftrag',
    ],
];
