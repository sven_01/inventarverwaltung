<?php

return [
    "model" => "Boxen",
    "model_singular" => "Box",
    "add" => "Box hinzufügen",
    "create" => "Box erstellen",
    "edit" => "Box bearbeiten",
    "list" => "Box Liste",
    'user' => 'Nutzer',
    'user_id_helper' => '',
    'user_helper' => '',
    'products' => 'Produkte',
    'product' => 'Produkte',
    'products_helper' => '',
    'box_template' => 'Box-Vorlage',
    'box_template_helper' => '',
    'target' => 'Menge (Soll)',
    'actual' => 'Menge (Ist)',
    'title' => 'Bezeichnung',
    'title_helper' => '',
    'update_inventory' => 'Lagerbestand aktualisieren',
    'back_to_box' => 'Zurück zur Box',
];
