@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('products.edit') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("products.update", [$product->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="title">{{ trans('products.title') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', $product->title) }}" required>
                @if($errors->has('title'))
                    <div class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('products.title_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="number">{{ trans('products.number') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="number" value="{{ old('number', $product->number) }}" required>
                @if($errors->has('number'))
                    <div class="invalid-feedback">
                        {{ $errors->first('number') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('products.number_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('products.description') }}</label>
                <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', $product->description) }}">
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('products.description_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="quantity">{{ trans('products.quantity') }}</label>
                <input class="form-control {{ $errors->has('quantity') ? 'is-invalid' : '' }}" type="text" name="quantity" id="quantity" value="{{ old('quantity', $product->quantity) }}">
                @if($errors->has('quantity'))
                    <div class="invalid-feedback">
                        {{ $errors->first('quantity') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('products.quantity_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="quantity_alert">{{ trans('products.quantity_alert') }}</label>
                <input class="form-control {{ $errors->has('quantity_alert') ? 'is-invalid' : '' }}" type="text" name="quantity_alert" id="quantity_alert" value="{{ old('quantity_alert', $product->quantity_alert) }}">
                @if($errors->has('quantity_alert'))
                    <div class="invalid-feedback">
                        {{ $errors->first('quantity_alert') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('products.quantity_alert_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('general.save') }}
                </button>
                <a class="btn btn-secondary" href="{{ route('products.index') }}">
                    {{ trans('general.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>

@endSection
