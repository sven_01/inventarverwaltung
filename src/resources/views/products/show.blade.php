@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('products.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('products.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('products.title') }}
                        </th>
                        <td>
                            {{ $product->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('products.description') }}
                        </th>
                        <td>
                            {{ $product->description }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('products.quantity') }}
                        </th>
                        <td>
                            {{ $product->quantity }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('products.quantity_alert') }}
                        </th>
                        <td>
                            {{ $product->quantity_alert }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <h4 class="text-center">
                @if(count($product->transactions) == 0)
                {{ __('transactions.empty') }}
                @else
                {{ __('transactions.model') }}
                @endif
            </h4>
            @if(count($product->transactions) > 0)
                <table class="table table-sm table-bordered table-striped col-6 m-auto">
                    <thead>
                        <tr>
                            <th class="w-75">{{ __('users.model') }}</th>
                            <th>{{ __('transactions.quantity') }}</th>
                            <th>{{ __('transactions.description') }}</th>
                            <th>{{ __('transactions.created_at') }}</th>
                        </tr>
                        @foreach($product->transactions as $transaction)
                            <tr>
                                <td>
                                    {{ $transaction->user->name }}
                                    ({{ $transaction->user->email }})
                                </td>
                                <td>{{ $transaction->quantity }}</td>
                                <td>{{ $transaction->description }}</td>
                                <td>{{ date("d.m.Y H:i", strtotime($transaction->created_at)) }}</td>
                            </tr>
                        @endforeach
                    </thead>
                </table>
            @endif
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('products.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endSection
