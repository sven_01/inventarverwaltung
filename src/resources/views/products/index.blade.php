@extends('layouts.app')
@section('content')

<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success {{auth()->user()->can("create products") ? '' : 'disabled'}}" href="{{ route("products.create") }}">
            {{ trans('products.add') }}
        </a>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('products.list') }}
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover datatable datatable-role">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('products.title') }}
                        </th>
                        <th>
                            {{ trans('products.description') }}
                        </th>
                        <th class="col-1">
                            {{ trans('products.quantity') }}
                        </th>
                        <th class="col-1">
                            {{ trans('products.quantity_alert') }}
                        </th>
                        <th class="col-1">
                            {{ trans('products.quantity_add') }}
                        </th>
                        <th class="col-1">
                            {{ trans('products.quantity_remove') }}
                        </th>
                        <th class="col-1">
                            {{ trans('transactions.description') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $key => $product)
                    <tr data-entry-id="{{ $product->id }}">
                        <form method="POST" action="{{ route("products.update", [$product->id]) }}" style="display: inline-block;" class="form-inline">
                            @method('PUT')
                            @csrf
                            <input type="hidden" name="title" value="{{$product->title}}">
                            <input type="hidden" name="quantity" value="{{$product->quantity}}">
                        <td>

                        </td>
                        <td>
                            {{ $product->title ?? '' }} ({{$product->number}})
                        </td>
                        <td>
                            {{ $product->description ?? '' }}
                        </td>
                        <td class="col-1">
                            {{ $product->quantity ?? '' }}
                        </td>
                        <td class="col-1">
                            {{ $product->quantity_alert ?? '' }}
                        </td>
                        <td class="col-1">
                            <input name="quantity_add" type="text" class="form-control form-control-sm col-6" min="1"/>
                        </td>
                        <td class="col-1">
                            <input name="quantity_remove" type="text" class="form-control form-control-sm col-6" min="1"/>
                        </td>
                        <td>
                            <div class="form-inline">
                                <input name="description_transaction" type="text" class="form-control form-control-sm col-6" min="1"/>
                                <input value="{{__('general.Send')}}" type="submit" class="btn btn-danger btn-xs ml-1"/>
                            </div>
                        </td>
                        </form>
                        <td>
                            <a class="btn btn-xs btn-primary {{auth()->user()->can("view products") ? '' : 'disabled'}}" href="{{ route('products.show', $product->id) }}">
                                {{ trans('general.view') }}
                            </a>

                            <a class="btn btn-xs btn-info {{auth()->user()->can("edit products") ? '' : 'disabled'}}" href="{{ route('products.edit', $product->id) }}">
                                {{ trans('general.edit') }}
                            </a>

                            <form action="{{ route('products.destroy', $product->id) }}" method="POST" onsubmit="return confirm('{{ trans('general.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-xs btn-danger {{auth()->user()->can("delete products") ? '' : 'disabled'}}" value="{{ trans('general.delete') }}">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endSection
