@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('customers.model') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('customers.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('customers.company') }}
                        </th>
                        <td>
                            {{ $customer->company }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('customers.first_name') }}
                        </th>
                        <td>
                            {{ $customer->first_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('customers.last_name') }}
                        </th>
                        <td>
                            {{ $customer->last_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('customers.street') }}
                        </th>
                        <td>
                            {{ $customer->street }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('customers.zip') }}
                        </th>
                        <td>
                            {{ $customer->zip }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('customers.city') }}
                        </th>
                        <td>
                            {{ $customer->city }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('customers.email') }}
                        </th>
                        <td>
                            {{ $customer->email }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('customers.phone') }}
                        </th>
                        <td>
                            {{ $customer->phone }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('customers.website') }}
                        </th>
                        <td>
                            {{ $customer->website }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('customers.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endSection
