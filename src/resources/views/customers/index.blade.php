@extends('layouts.app')
@section('content')

<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success {{auth()->user()->can("create customers") ? '' : 'disabled'}}" href="{{ route("customers.create") }}">
            {{ trans('customers.add') }}
        </a>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('customers.list') }}
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('customers.company') }}
                        </th>
                        <th>
                            {{ trans('customers.first_name') }}
                        </th>
                        <th>
                            {{ trans('customers.last_name') }}
                        </th>
                        <th>
                            {{ trans('customers.email') }}
                        </th>
                        <th>
                            {{ trans('customers.phone') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($customers as $key => $customer)
                        <tr data-entry-id="{{ $customer->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $customer->company ?? '' }}
                            </td>
                            <td>
                                {{ $customer->first_name ?? '' }}
                            </td>
                            <td>
                                {{ $customer->last_name ?? '' }}
                            </td>
                            <td>
                                @if($customer->email)
                                <a href = "mailto: {{ $customer->email }}">{{ $customer->email }}</a>
                                @endif
                            </td>
                            <td>
                                {{ $customer->phone ?? '' }}
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary {{auth()->user()->can("view customers") ? '' : 'disabled'}}" href="{{ route('customers.show', $customer->id) }}">
                                    {{ trans('general.view') }}
                                </a>

                                <a class="btn btn-xs btn-info {{auth()->user()->can("edit customers") ? '' : 'disabled'}}" href="{{ route('customers.edit', $customer->id) }}">
                                    {{ trans('general.edit') }}
                                </a>

                                <form action="{{ route('customers.destroy', $customer->id) }}" method="POST" onsubmit="return confirm('{{ trans('general.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger {{auth()->user()->can("delete customers") ? '' : 'disabled'}}" value="{{ trans('general.delete') }}">
                                </form>

                                <a class="btn btn-xs btn-success" {{auth()->user()->can("create orders") ? '' : 'disabled'}} href="{{"/orders/create?cid=".$customer->id}}">
                                    {{ trans('orders.create') }}
                                </a>
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endSection
