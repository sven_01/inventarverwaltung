@extends('layouts.app')
@section('content')

<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success {{auth()->user()->can("create orders") ? '' : 'disabled'}}" href="{{ route("orders.create") }}">
            {{ trans('orders.add') }}
        </a>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('orders.list') }}
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('orders.title') }}
                        </th>
                        <th>
                            {{ trans('orders.customer') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $key => $order)
                        <tr data-entry-id="{{ $order->id }}">
                            <td>

                            </td>
                            <td>
                               {{ $order->title }}
                               @if(!empty($order->order_nr))
                               ({{ $order->order_nr }})
                               @endif
                            </td>
                            <td>
                                <a href="/customers/{{$order->customer->id}}">{{ $order->customer->company ?? $order->customer->last_name }}</a>
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary {{auth()->user()->can("view orders") ? '' : 'disabled'}}" href="{{ route('orders.show', $order->id) }}">
                                    {{ trans('general.view') }}
                                </a>

                                <a class="btn btn-xs btn-info {{auth()->user()->can("edit orders") ? '' : 'disabled'}}" href="{{ route('orders.edit', $order->id) }}">
                                    {{ trans('general.edit') }}
                                </a>
                                <form action="{{ route('orders.destroy', $order->id) }}" method="POST" onsubmit="return confirm('{{ trans('general.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger {{auth()->user()->can("delete orders") ? '' : 'disabled'}}" value="{{ trans('general.delete') }}">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endSection
