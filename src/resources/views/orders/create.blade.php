@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('orders.create') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("orders.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="title">{{ trans('orders.title') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" value="{{ old('title', '') }}" required>
                @if($errors->has('title'))
                    <div class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label class="required" for="customer_id">{{ trans('orders.customer') }}</label>
                <select class="form-control select2 {{ $errors->has('customer_id') ? 'is-invalid' : '' }}" name="customer_id" required>
                    @foreach($customers as $id => $customer)
                        <option value="{{ $id }}" {{ (old('customer_id') == $id || (isset($customerModel) && $customerModel->id == $id)) ? 'selected' : '' }}>{{ $customer }}</option>
                    @endforeach
                </select>
                @if($errors->has('customer_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('customer_id') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label for="order_nr">{{ trans('orders.order_nr') }}</label>
                <input class="form-control {{ $errors->has('order_nr') ? 'is-invalid' : '' }}" type="text" name="order_nr" value="{{ old('order_nr', '') }}" required>
                @if($errors->has('order_nr'))
                    <div class="invalid-feedback">
                        {{ $errors->first('order_nr') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('general.save') }}
                </button>
                <a class="btn btn-secondary" href="{{ route('orders.index') }}">
                    {{ trans('general.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>



@endsection
