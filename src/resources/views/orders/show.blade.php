@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('orders.model') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('orders.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
            </div>
            <h4>
                {{trans('general.general_information')}}
            </h4>
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('orders.title') }}
                        </th>
                        <td>
                            {{ $order->title }}
                        </td>
                    </tr>
                    @if(!empty($order->order_nr))
                    <tr>
                        <th>
                            {{ trans('orders.order_nr') }}
                        </th>
                        <td>
                            {{ $order->order_nr }}
                        </td>
                    </tr>
                    @endif
                    <tr>
                        <th>
                            {{ trans('orders.customer') }}
                        </th>
                        <td>
                            <a href="/customers/{{$order->customer->id}}">{{ $order->customer->name }}</a>
                        </td>
                    </tr>
                </tbody>
            </table>

            <h4>
                {{trans('general.product_list')}}
            </h4>
            <!-- products -->
            @if ($order->products && count($order->products) > 0)
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>
                            {{ trans('orders.product') }}
                        </th>
                        <th>
                            {{ trans('orders.actual') }}
                        </th>
                        <th>
                            {{ trans('orders.target') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($order->products as $key => $product)
                    <tr>
                        <td>
                            {{ $product->title }}
                        </td>
                        <td>
                            {{$product->pivot->actual}}
                        </td>
                        <td>
                            {{$product->pivot->target}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <p><strong>{{ trans('products.no_products') }}</strong></p>
            @endif
            @if(count($productsRemaining) > 0)
                <strong>{{trans('orders.missing_products_full')}}</strong>
                <table class="table table-sm table-bordered table-striped">
                <thead>
                    <tr>
                        <th>{{ __('orders.product') }}</th>
                        <th>{{ __('orders.quantity_missing') }}</th>
                    </tr>
                    @foreach($productsRemaining as $productName => $quantity)
                        <tr>
                            <td>{{ $productName }}</td>
                            <td>{{ $quantity }}</td>
                        </tr>
                    @endforeach
                </thead>
            </table>
            <div class="form-group">
                <form action="{{ route('orders.show', $order->id) }}">
                    <input type="hidden" name="add_missing_products_to_box" value="1">
                    <button type="submit" class="btn btn-success btn-sm" ">
                        {{ trans('orders.add_missing_products_to_box') }}
                    </button>
                </form>
            </div>
            @endif
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('orders.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endSection
