@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('boxes.create') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("boxes.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('boxes.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user_id') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('boxes.user_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="box_template_id">{{ trans('boxes.box_template') }}</label>
                <select class="form-control select2 {{ $errors->has('box_template_id') ? 'is-invalid' : '' }}" name="box_template_id" id="box_template_id" required>
                    @foreach($boxTemplates as $id => $boxTemplate)
                        <option value="{{ $id }}" {{ old('box_template_id') == $id ? 'selected' : '' }}>{{ $boxTemplate }}</option>
                    @endforeach
                </select>
                @if($errors->has('box_template_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('box_template_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('boxes.box_template_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('general.save') }}
                </button>
                <a class="btn btn-secondary" href="{{ route('boxes.index') }}">
                    {{ trans('general.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>



@endsection
