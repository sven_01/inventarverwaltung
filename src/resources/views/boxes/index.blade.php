@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('boxes.list') }}
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('boxes.user') }}
                        </th>
                        <th>
                            {{ trans('boxes.box_template') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($boxes as $key => $box)
                        <tr data-entry-id="{{ $box->id }}">
                            <td>

                            </td>
                            <td>
                                <a href="/users/{{$box->user->id}}"> {{ $box->user->name ?? '' }}</a>
                            </td>
                            <td>
                                @if(isset($box->boxTemplate->id))
                                <a href="/boxTemplates/{{$box->boxTemplate->id}}">{{ $box->boxTemplate->title ?? '' }}</a>
                                @endif
                            </td>
                            <td>
                                    <a class="btn btn-xs btn-primary {{auth()->user()->can("view boxes") ? '' : 'disabled'}}" href="{{ route('boxes.show', $box->id) }}">
                                        {{ trans('general.view') }}
                                    </a>

                                    <a class="btn btn-xs btn-info {{auth()->user()->can("edit boxes") ? '' : 'disabled'}}" href="{{ route('boxes.edit', $box->id) }}">
                                        {{ trans('general.edit') }}
                                    </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endSection
