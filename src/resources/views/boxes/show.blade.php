@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        @can('admin')
        {{ trans('boxes.model') }}
        @endcan
        @can('user')
        {{ trans('boxes.model_singular') }}
        @endcan
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                @can('admin')
                <a class="btn btn-default" href="{{ route('boxes.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
                @endcan
                @can('user')
                <a class="btn btn-danger" href="{{ route('boxes.edit',$box->id) }}">
                    {{ trans('general.edit') }}
                </a>
                @endcan
            </div>
            <h4>
                {{trans('general.general_information')}}
            </h4>
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('boxes.user') }}
                        </th>
                        <td>
                            {{ $box->user->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('boxes.box_template') }}
                        </th>
                        <td>
                            @if(isset($box->boxTemplate))
                            {{ $box->boxTemplate->title }}
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>

            <h4>
                {{trans('general.product_list')}}
            </h4>
            <!-- products -->
            @if ($box->products && count($box->products) > 0)
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>
                            {{ trans('boxes.product') }}
                        </th>
                        <th>
                            {{ trans('boxes.actual') }}
                        </th>
                        <th>
                            {{ trans('boxes.target') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($box->products as $key => $product)
                    <tr>
                        <td>
                            {{ $product->title }}
                        </td>
                        <td>
                            {{$product->pivot->actual}}
                        </td>
                        <td>
                            {{$product->pivot->target}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <p><strong>{{ trans('products.no_products') }}</strong></p>
            @endif
            <div class="form-group">
                @can('admin')
                <a class="btn btn-default" href="{{ route('boxes.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
                @endcan
                @can('user')
                <a class="btn btn-danger" href="{{ route('boxes.edit',$box->id) }}">
                    {{ trans('general.edit') }}
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>

@endSection
