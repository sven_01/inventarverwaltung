@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('boxes.edit') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("boxes.update", [$box->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            @can("admin")
            <div class="form-group">
                <label class="required" for="box_template_id">{{ trans('boxes.box_template') }}</label>
                <select class="form-control select2 {{ $errors->has('box_template_id') ? 'is-invalid' : '' }}" name="box_template_id" id="box_template_id" required>
                    @foreach($boxTemplates as $id => $boxTemplate)
                        <option value="{{ $id }}" {{ ($box->box_template_id == $id ||  old('box_template_id') == $id) ? 'selected' : '' }}>{{ $boxTemplate }}</option>
                    @endforeach
                </select>
                @if($errors->has('box_template_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('box_template_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('boxes.box_template_helper') }}</span>
            </div>
            @endcan
            @include('components.productSelect',["model" => $box])
            <div class="table-responsive">
                <table id="productsTable" class="table table-sm table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="w-7">{{ __('boxes.product') }}</th>
                            <th>{{ __('boxes.actual') }}</th>
                            <th>{{ __('boxes.target') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($box->products as $product)
                            <tr id="product_row_{{$product->id}}">
                                <td>
                                    {{ $product->title }}
                                </td>
                                <td><div class="input-group"><input class="form-control" name="actuals[{{$product->id}}]" id="actual_{{$product->id}}" type="number" value="{{ $product->pivot->actual ?? 0}}" step="1" min="0"><div class="input-group-append"><button class="btn btn-outline-primary" type="button" style="touch-action: manipulation;" onclick="document.getElementById(&quot;actual_{{$product->id}}&quot;).stepUp();document.getElementById(&quot;actual_{{$product->id}}&quot;).dispatchEvent(new Event('change'))">+</button><button class="btn btn-outline-primary" type="button" style="touch-action: manipulation;" onclick="document.getElementById(&quot;actual_{{$product->id}}&quot;).stepDown();document.getElementById(&quot;actual_{{$product->id}}&quot;).dispatchEvent(new Event('change'))">-</button></div></div></div></div></td>
                                <td><div class="input-group"><input class="form-control" name="targets[{{$product->id}}]" id="target_{{$product->id}}" type="number" value="{{ $product->pivot->target ?? 0}}" step="1" min="0"><div class="input-group-append"><button class="btn btn-outline-primary" type="button" style="touch-action: manipulation;" onclick="document.getElementById(&quot;target_{{$product->id}}&quot;).stepUp();document.getElementById(&quot;target_{{$product->id}}&quot;).dispatchEvent(new Event('change'))">+</button><button class="btn btn-outline-primary" type="button" style="touch-action: manipulation;" onclick="document.getElementById(&quot;target_{{$product->id}}&quot;).stepDown();document.getElementById(&quot;target_{{$product->id}}&quot;).dispatchEvent(new Event('change'))">-</button></div></div></div></div></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="form-check mb-2">
                    <input type="checkbox" class="form-check-input" checked name="update_inventory" value="1">
                    <label class="form-check-label" for="exampleCheck1">{{trans('boxes.update_inventory')}}</label>
                    <small class="form-text text-muted">Wenn aktiviert: Hinzugefügte Produkte reduzieren automatisch den Lagerbestand. Entfernte Produkte erhöhen den Lagerbestand.</small>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('general.save') }}
                </button>
                <a class="btn btn-secondary" href="{{ Gate::check('admin') ? route('boxes.index') : route('boxes.show',$box->id)}}">
                    {{ trans('general.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>

@endSection

@section('scripts')

<script>
    window.useQrScanner = "true"

    var cell2HTML ='<div class="input-group"><input class="form-control" name="actuals[]" type="number" value="0" step="1" min="0"><div class="input-group-append"><button class="btn btn-outline-primary" type="button">+</button><button class="btn btn-outline-primary" type="button">-</button></div></div></div></div>'
    var cell3HTML ='<div class="input-group"><input class="form-control" name="targets[]" type="number" value="1" step="1" min="0"><div class="input-group-append"><button class="btn btn-outline-primary" type="button">+</button><button class="btn btn-outline-primary" type="button">-</button></div></div></div></div>'

    var table = document.getElementById('productsTable')

    $('#products').on('select2:select', function (e) {
    console.log(e)
        var selectedProductName = e.params.data.element.innerHTML;
        var selectedProductId = e.params.data.element.value;
        var newRowId = 'product_row_'+selectedProductId
        var exisitingRow = document.getElementById(newRowId)
        if(!exisitingRow){
            var newRow = table.insertRow()
            newRow.id = newRowId
            var cell1 = newRow.insertCell()
            cell1.innerHTML = selectedProductName

            var cell2 = newRow.insertCell()
            cell2.innerHTML = cell2HTML
            var input2 =  cell2.getElementsByTagName('input')[0]
            input2.name = `actuals[${selectedProductId}]`
            input2.id = 'actual_'+selectedProductId
            var button1 =  cell2.getElementsByTagName('button')[0]
            button1.addEventListener('click', () => {input2.stepUp();input2.dispatchEvent(new Event('change'))});
            button1.style.cssText += "touch-action: manipulation;"//prevent double tap
            var button2 =  cell2.getElementsByTagName('button')[1]
            button2.addEventListener('click', () => {input2.stepDown(); input2.dispatchEvent(new Event('change'))});
            button2.style.cssText += "touch-action: manipulation;"//prevent double tap
            input2.addEventListener('change', event => {
                var actual = event.target
                var actualId = actual.id
                var targetId = actualId.replace("actual","target")
                var target = document.getElementById(targetId)
                if(parseFloat(actual.value) > parseFloat(target.value)){
                    target.value = actual.value
                }
            })

            var cell3 = newRow.insertCell()
            cell3.innerHTML = cell3HTML
            var input3 =  cell3.getElementsByTagName('input')[0]
            input3.name = `targets[${selectedProductId}]`
            input3.id = 'target_'+selectedProductId
            var button1 =  cell3.getElementsByTagName('button')[0]
            button1.addEventListener('click', () => {input3.stepUp();  input3.dispatchEvent(new Event('change'))});
            button1.style.cssText += "touch-action: manipulation;"//prevent double tap
            var button2 =  cell3.getElementsByTagName('button')[1]
            button2.addEventListener('click', () => {input3.stepDown(); input3.dispatchEvent(new Event('change'))});
            button2.style.cssText += "touch-action: manipulation;"//prevent double tap
        }
    });
    $('#products').on('select2:unselect', function (e) {
        var selectedProductId = e.params.data.element.value;
        var selectedProductRowId = "product_row_"+selectedProductId
        var selectedProductRow = document.getElementById(selectedProductRowId)
        if(selectedProductRow){
            selectedProductRow.parentNode.removeChild(selectedProductRow)
        }
    });
    $('.deselect-all').click(function () {
        while(table.rows.length > 1){
            table.deleteRow(-1)
        }
    })

    function updateListeners(){
        document.querySelectorAll('[id*="actual_"]').forEach(item => {
            item.addEventListener('change', event => {
                var actual = event.target
                var actualId = actual.id
                var targetId = actualId.replace("actual","target")
                var target = document.getElementById(targetId)
                if(parseFloat(actual.value) > parseFloat(target.value)){
                    target.value = actual.value
                }
            })
        })
    }
    updateListeners()
</script>

@endsection
