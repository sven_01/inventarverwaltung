@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('boxTemplates.model') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('boxTemplates.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
            </div>
            <p><strong>{{ $boxTemplate->title }}</strong></p>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>
                            {{ trans('boxTemplates.product') }}
                        </th>
                        <th>
                            {{ trans('boxTemplates.target') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($boxTemplate->products as $key => $product)
                    <tr>
                        <td>
                            {{ $product->title }}
                        </td>
                        <td>
                            {{$product->pivot->target}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('boxTemplates.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endSection
