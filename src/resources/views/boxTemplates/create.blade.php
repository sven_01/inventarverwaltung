@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('boxTemplates.create') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("boxTemplates.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="title">{{ trans('boxTemplates.title') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', '') }}" required>
                @if($errors->has('title'))
                    <div class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('boxTemplates.title_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="products">{{ trans('boxTemplates.products') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('general.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('products') ? 'is-invalid' : '' }}" name="products[]" id="products" multiple required>
                    @foreach($products as $id => $product)
                        <option value="{{ $id }}" {{ in_array($id, old('products', [])) ? 'selected' : '' }}>{{ $product }}</option>
                    @endforeach
                </select>
                @if($errors->has('products'))
                    <div class="invalid-feedback">
                        {{ $errors->first('products') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('boxTemplates.products_helper') }}</span>
            </div>
            <div class="table-responsive">
                <table id="productsTable" class="table table-sm table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="w-7">{{ __('boxTemplates.product') }}</th>
                            <th>{{ __('boxTemplates.target') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('general.save') }}
                </button>
                <a class="btn btn-secondary" href="{{ route('boxTemplates.index') }}">
                    {{ trans('general.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')

<script>

    var cell2HTML ='<input class="form-control" name="targets[]" type="number" value="0" min="0">'
    var table = document.getElementById('productsTable')

        $('.select2').on('select2:select', function (e) {
            var selectedProductName = e.params.data.element.innerHTML;
            var selectedProductId = e.params.data.element.value;
            var newRowId = 'product_row_'+selectedProductId
            var exisitingRow = document.getElementById(newRowId)
            if(!exisitingRow){
                var newRow = table.insertRow()
                newRow.id = newRowId
                var cell1 = newRow.insertCell()
                cell1.innerHTML = selectedProductName
                var cell2 = newRow.insertCell()
                cell2.innerHTML = cell2HTML
                cell2.children[0].name = `targets[${selectedProductId}]`
            }
        });
        $('.select2').on('select2:unselect', function (e) {
            var selectedProductId = e.params.data.element.value;
            var selectedProductRowId = "product_row_"+selectedProductId
            var selectedProductRow = document.getElementById(selectedProductRowId)
            if(selectedProductRow){
                selectedProductRow.parentNode.removeChild(selectedProductRow)
            }
        });
        $('.deselect-all').click(function () {
           while(table.rows.length > 1){
               table.deleteRow(-1)
           }
        })

</script>

@endsection
