@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('boxTemplates.edit') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("boxTemplates.update", [$boxTemplate->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('boxTemplates.title') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', $boxTemplate->title) }}" required>
                @if($errors->has('title'))
                    <div class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('boxTemplates.title_helper') }}</span>
            </div>
            @include('components.productSelect',["model" => $boxTemplate])
            <div class="table-responsive">
                <table id="productsTable" class="table table-sm table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="w-7">{{ __('boxTemplates.product') }}</th>
                            <th>{{ __('boxTemplates.target') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($boxTemplate->products as $product)
                            <tr id="product_row_{{$product->id}}">
                                <td>
                                    {{ $product->title }}
                                </td>
                                <td><div class="input-group"><input class="form-control" name="targets[{{$product->id}}]" id="target_{{$product->id}}" type="number" value="{{ $product->pivot->target ?? 0}}" step="1" min="0"><div class="input-group-append"><button class="btn btn-outline-primary" type="button" style="touch-action: manipulation;" onclick="document.getElementById(&quot;target_{{$product->id}}&quot;).stepUp();document.getElementById(&quot;target_{{$product->id}}&quot;).dispatchEvent(new Event('change'))">+</button><button class="btn btn-outline-primary" type="button" style="touch-action: manipulation;" onclick="document.getElementById(&quot;target_{{$product->id}}&quot;).stepDown();document.getElementById(&quot;target_{{$product->id}}&quot;).dispatchEvent(new Event('change'))">-</button></div></div></div></div></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('general.save') }}
                </button>
                <a class="btn btn-secondary" href="{{ route('boxTemplates.index') }}">
                    {{ trans('general.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>

@endSection

@section('scripts')

<script>
    window.useQrScanner = "true"

    var cell2HTML ='<div class="input-group"><input class="form-control" name="targets[]" type="number" value="1" step="1" min="0"><div class="input-group-append"><button class="btn btn-outline-primary" type="button">+</button><button class="btn btn-outline-primary" type="button">-</button></div></div></div></div>'

    var table = document.getElementById('productsTable')

    $('.select2').on('select2:select', function (e) {
        var selectedProductName = e.params.data.element.innerHTML;
        var selectedProductId = e.params.data.element.value;
        var newRowId = 'product_row_'+selectedProductId
        var exisitingRow = document.getElementById(newRowId)
        if(!exisitingRow){
            var newRow = table.insertRow()
            newRow.id = newRowId
            var cell1 = newRow.insertCell()
            cell1.innerHTML = selectedProductName

            var cell2 = newRow.insertCell()
            cell2.innerHTML = cell2HTML
            var input3 =  cell2.getElementsByTagName('input')[0]
            input3.name = `targets[${selectedProductId}]`
            input3.id = 'target_'+selectedProductId
            var button1 =  cell2.getElementsByTagName('button')[0]
            button1.addEventListener('click', () => {input3.stepUp();  input3.dispatchEvent(new Event('change'))});
            button1.style.cssText += "touch-action: manipulation;"//prevent double tap
            var button2 =  cell2.getElementsByTagName('button')[1]
            button2.addEventListener('click', () => {input3.stepDown(); input3.dispatchEvent(new Event('change'))});
            button2.style.cssText += "touch-action: manipulation;"//prevent double tap
        }
    });
    $('.select2').on('select2:unselect', function (e) {
        var selectedProductId = e.params.data.element.value;
        var selectedProductRowId = "product_row_"+selectedProductId
        var selectedProductRow = document.getElementById(selectedProductRowId)
        if(selectedProductRow){
            selectedProductRow.parentNode.removeChild(selectedProductRow)
        }
    });
    $('.deselect-all').click(function () {
        while(table.rows.length > 1){
            table.deleteRow(-1)
        }
    })

</script>

@endsection
