@extends('layouts.app')
@section('content')

<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success {{auth()->user()->can("create boxTemplates") ? '' : 'disabled'}}" href="{{ route("boxTemplates.create") }}">
            {{ trans('boxTemplates.add') }}
        </a>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('boxTemplates.list') }}
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('boxTemplates.title') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($boxTemplates as $key => $boxTemplate)
                        <tr data-entry-id="{{ $boxTemplate->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $boxTemplate->title ?? '' }}
                            </td>
                            <td>
                                    <a class="btn btn-xs btn-primary {{auth()->user()->can("view boxTemplates") ? '' : 'disabled'}}" href="{{ route('boxTemplates.show', $boxTemplate->id) }}">
                                        {{ trans('general.view') }}
                                    </a>

                                    <a class="btn btn-xs btn-info {{auth()->user()->can("edit boxTemplates") ? '' : 'disabled'}}" href="{{ route('boxTemplates.edit', $boxTemplate->id) }}">
                                        {{ trans('general.edit') }}
                                    </a>

                                    <form action="{{ route('boxTemplates.destroy', $boxTemplate->id) }}" method="POST" onsubmit="return confirm('{{ trans('general.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger {{auth()->user()->can("delete boxTemplates") ? '' : 'disabled'}}" value="{{ trans('general.delete') }}">
                                    </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endSection
