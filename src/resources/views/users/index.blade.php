@extends('layouts.app')
@section('content')

<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success {{auth()->user()->can("create users") ? '' : 'disabled'}}" href="{{ route("users.create") }}">
            {{ trans('users.add') }}
        </a>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('users.list') }}
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('users.name') }}
                        </th>
                        <th>
                            {{ trans('users.email') }}
                        </th>
                        <th>
                            {{ trans('users.roles') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
                        <tr data-entry-id="{{ $user->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $user->name ?? '' }}
                            </td>
                            <td>
                                {{ $user->email ?? '' }}
                            </td>
                            <td>
                                @foreach($user->roles as $key => $item)
                                    <span class="badge badge-info">{{ $item->name }}</span>
                                @endforeach
                            </td>
                            <td>
                                    <a class="btn btn-xs btn-primary {{auth()->user()->can("view users") ? '' : 'disabled'}}" href="{{ route('users.show', $user->id) }}">
                                        {{ trans('general.view') }}
                                    </a>

                                    <a class="btn btn-xs btn-info {{auth()->user()->can("edit users") ? '' : 'disabled'}}" href="{{ route('users.edit', $user->id) }}">
                                        {{ trans('general.edit') }}
                                    </a>

                                    <form action="{{ route('users.destroy', $user->id) }}" method="POST" onsubmit="return confirm('{{ trans('general.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger {{auth()->user()->can("delete users") ? '' : 'disabled'}}" value="{{ trans('general.delete') }}">
                                    </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endSection
