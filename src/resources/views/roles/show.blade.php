@extends('layouts.app')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('roles.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('roles.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('roles.name') }}
                        </th>
                        <td>
                            {{ $role->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('roles.permissions') }}
                        </th>
                        <td>
                            @foreach($role->permissions as $key => $permission)
                                <span class="badge badge-info">{{ $permission->name }}</span>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('roles.index') }}">
                    {{ trans('general.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endSection
