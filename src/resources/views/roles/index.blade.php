@extends('layouts.app')
@section('content')

<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success {{auth()->user()->can("create roles") ? '' : 'disabled'}}" href="{{ route("roles.create") }}">
            {{ trans('roles.add') }}
        </a>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('roles.list') }}
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover datatable datatable-role">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('roles.name') }}
                        </th>
                        <th>
                            {{ trans('roles.permissions') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles as $key => $role)
                        <tr data-entry-id="{{ $role->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $role->name ?? '' }}
                            </td>
                            <td>
                                @foreach($role->permissions as $key => $item)
                                    <span class="badge badge-info">{{ $item->name }}</span>
                                @endforeach
                            </td>
                            <td>
                                    <a class="btn btn-xs btn-primary {{auth()->user()->can("view roles") ? '' : 'disabled'}}" href="{{ route('roles.show', $role->id) }}">
                                        {{ trans('general.view') }}
                                    </a>

                                    <a class="btn btn-xs btn-info {{auth()->user()->can("edit roles") ? '' : 'disabled'}}" href="{{ route('roles.edit', $role->id) }}">
                                        {{ trans('general.edit') }}
                                    </a>

                                    <form action="{{ route('roles.destroy', $role->id) }}" method="POST" onsubmit="return confirm('{{ trans('general.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger {{auth()->user()->can("delete roles") ? '' : 'disabled'}}" value="{{ trans('general.delete') }}">
                                    </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endSection
