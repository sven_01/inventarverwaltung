<div class="sidebar">
    <nav class="sidebar-nav">

        <ul class="nav">
            <!-- User Management -->
            @can("user_management")
            <li class="nav-item nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users nav-icon">

                    </i>
                    {{ trans('users.user_management') }}
                </a>
                <ul class="nav-dropdown-items">
                    @can("view roles")
                    <li class="nav-item">
                        <a href="{{ route("roles.index") }}" class="nav-link {{ request()->is('/roles') || request()->is('*roles/*') ? 'active' : '' }}">
                            <i class="fa-fw fas fa-briefcase nav-icon">

                            </i>
                            {{ trans('roles.model') }}
                        </a>
                    </li>
                    @endcan
                    @can("view users")
                    <li class="nav-item">
                        <a href="{{ route("users.index") }}" class="nav-link {{ request()->is('/users') || request()->is('*users/*') ? 'active' : '' }}">
                            <i class="fa-fw fas fa-user nav-icon">

                            </i>
                            {{ trans('users.model') }}
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can("view products")
            <li class="nav-item">
                <a href="{{ route("products.index") }}" class="nav-link {{ request()->is('/products') || request()->is('*products/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-warehouse nav-icon">

                    </i>
                    {{ trans('products.model') }}
                </a>
            </li>
            @endcan
            @can("view boxTemplates")
            <li class="nav-item">
                <a href="{{ route("boxTemplates.index") }}" class="nav-link {{ request()->is('/boxTemplates') || request()->is('*boxTemplates/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-box nav-icon">

                    </i>
                    {{ trans('boxTemplates.model') }}
                </a>
            </li>
            @endcan
            @if(Gate::check('view boxes') && Gate::check('admin'))
            <li class="nav-item">
                <a href="{{ route("boxes.index") }}" class="nav-link {{ request()->is('/boxes') || request()->is('*boxes/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-box nav-icon">

                    </i>
                    {{ trans('boxes.model') }}
                </a>
            </li>
            @endif
            @if(Gate::check('view boxes') && Gate::check('user'))
            <li class="nav-item">
                <a href="{{ route("boxes.show",Auth::user()->box->id) }}" class="nav-link {{ request()->is('/boxes') || request()->is('*boxes/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-box nav-icon">

                    </i>
                    {{ trans('boxes.model_singular') }}
                </a>
            </li>
            @endif
            @can("view orders")
            <li class="nav-item">
                <a href="{{ route("orders.index") }}" class="nav-link {{ request()->is('/v') || request()->is('*orders/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-list nav-icon">

                    </i>
                    {{ trans('orders.model') }}
                </a>
            </li>
            @endcan
            @can("view customers")
            <li class="nav-item">
                <a href="{{ route("customers.index") }}" class="nav-link {{ request()->is('/customers') || request()->is('*customers/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-handshake nav-icon">

                    </i>
                    {{ trans('customers.model') }}
                </a>
            </li>
            @endcan
            <li class="nav-item">
                <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-fw fa-sign-out-alt">

                    </i>
                    {{ trans('general.logout') }}
                </a>
            </li>
            </li>
        </ul>

    </nav>
    <button class="sidebar-minimizer" type="button"></button>
</div>
