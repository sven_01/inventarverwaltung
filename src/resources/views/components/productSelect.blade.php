<div class="form-group">
    <label for="products">{{ trans('boxes.products') }}</label>
    <div style="padding-bottom: 4px">
        <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('general.deselect_all') }}</span>
        <span class="btn btn-danger btn-xs" style="border-radius: 0" id="startScannerBtn">{{ trans('general.qr_code') }}</span>
    </div>
    <select class="form-control select2 {{ $errors->has('products') ? 'is-invalid' : '' }}" name="products[]" id="products" multiple>
        @foreach($products as $id => $product)
            <option id="product_option_{{$id}}" value="{{ $id }}" {{ in_array($id, old('products', [])) || $model->products->contains($id) ? 'selected' : '' }}>{{ $product }}</option>
        @endforeach
    </select>
    @if($errors->has('products'))
        <div class="invalid-feedback">
            {{ $errors->first('products') }}
        </div>
    @endif
    <span class="help-block">{{ trans('boxes.products_helper') }}</span>
</div>
<div id="video-container" style="display:none" class="border border-secondary">
    <video id="video" muted playsinline class="w-100"></video>
</div>
