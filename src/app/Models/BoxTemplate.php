<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BoxTemplate extends Model
{
    use HasFactory;

    public $table = "box_templates";

    protected $guarded = [];

    public function products () {
        return $this->belongsToMany(Product::class)->orderBy('box_template_product.created_at')->withPivot(['target']);
    }

    public function boxes () {
        return $this->hasMany(Box::class);
    }
}
