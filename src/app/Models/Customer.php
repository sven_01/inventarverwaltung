<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    public $table = "customers";

    protected $guarded = [];

    public function orders () {
        return $this->hasMany(Order::class);
    }

    public function getNameAttribute()
    {
        return $this->company ?? $this->last_name;
    }
}
