<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Box extends Model
{
    use HasFactory;

    public $table = "boxes";

    protected $guarded = [];

    public function boxTemplate () {
        return $this->belongsTo(BoxTemplate::class);
    }

    public function products () {
        return $this->belongsToMany(Product::class)->withPivot(['target','actual']);
    }

    public function user () {
        return $this->belongsTo(User::class);
    }

}
