<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public $table = "orders";

    protected $guarded = [];

    public function products () {
        return $this->belongsToMany(Product::class)->withPivot(['target','actual']);
    }

    public function customer () {
        return $this->belongsTo(Customer::class);
    }
}
