<?php

namespace App\Models;

class Role extends \Spatie\Permission\Models\Role implements \Spatie\Permission\Contracts\Role
{
    public $table = 'roles';
}
