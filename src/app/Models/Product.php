<?php

namespace App\Models;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    public $table = "products";

    protected $guarded = [];

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction','product_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class)->withPivot(['quantity','delivery_status']);
    }

    public function boxTemplates()
    {
        return $this->belongsToMany(BoxTemplate::class)->withPivot(['target']);
    }

    public function boxes()
    {
        return $this->belongsToMany(Box::class)->withPivot(['target','actual']);
    }
}
