<?php

namespace App\Http\Controllers;

use App\Models\Box;
use App\Models\User;
use App\Models\Product;
use App\Models\BoxTemplate;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class BoxController extends Controller
{
    public function index(Request $request)
    {
        $boxes = Box::all();
        return view('boxes.index', compact('boxes'));
    }

    public function create(Request $request)
    {
        $boxTemplates = BoxTemplate::all()->pluck('title', 'id');
        $users = User::all()->pluck('name', 'id');
        return view('boxes.create',compact('boxTemplates','users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id'         => 'required|integer',
            'box_template_id'         => 'required|integer',
        ]);

        $input = $request->all();
        $box = Box::create($input);

        $boxTemplate = BoxTemplate::find($input['box_template_id']);
        $templateProducts = $boxTemplate->products;
        $productIdsAndPivotValues = [];
        foreach($templateProducts as $product){
            $productIdsAndPivotValues[$product->id] = ['target' => $product->pivot->target, 'actual' => 0];
        }
        $box->products()->sync($productIdsAndPivotValues);

        return redirect()->route('boxes.index');
    }

    public function show(Box $box)
    {
        return view('boxes.show',compact('box'));
    }

    public function edit(Box $box)
    {
        $products = Product::all()->pluck('title', 'id');
        $boxTemplates = BoxTemplate::all()->pluck('title', 'id');
        $users = User::all()->pluck('name', 'id');
        $productsNumberToId = json_encode(Product::all()->pluck('id', 'number'));
        return view('boxes.edit',compact('box','products','boxTemplates','users','productsNumberToId'));
    }

    public function update(Request $request, Box $box)
    {
        $this->validate($request, [
            'box_template_id' =>'nullable|integer',
            'products' => 'array',
            'user_id' => 'nullable|integer',
            'actuals' => 'array',
            'targets' => 'array',
            'update_inventory' => 'integer|digits:1',
        ]);

        $productIds = $request->input('products',[]);
        $productIdsAndPivotValues = [];
        $actuals = $request->input('actuals',[]);
        $targets = $request->input('targets',[]);

        $errors = [];

        foreach($productIds as $productId){
            $productIdsAndPivotValues[$productId] = ['actual' => $actuals[$productId], 'target' => $targets[$productId]];
        }

        $boxTemplateId = $request->input('box_template_id');
        if(isset($boxTemplateId) && $boxTemplateId != $box->box_template_id){
            $boxTemplate = BoxTemplate::find($boxTemplateId);
            $templateProducts = $boxTemplate->products;
            foreach($templateProducts as $templateProduct){
                if(isset($productIdsAndPivotValues[$templateProduct->id])){
                    $productIdsAndPivotValues[$templateProduct->id]['target'] = $templateProduct->pivot->target;
                } else {
                    $productIdsAndPivotValues[$templateProduct->id] = ['actual' => 0, 'target' => $templateProduct->pivot->target];
                }
            }
        }

        //update products stocks and create transactions
        if($request->input('update_inventory') === "1"){
            foreach($actuals as $productId => $actual){
                $product = $box->products()->where('box_product.product_id',$productId)->first();
                if(empty($product)){//new product
                    $product = Product::find($productId);
                    $quantityChange = $actual;
                } else {
                    $oldActual = $product->pivot->actual;
                    $quantityChange = floatval($actual) - $oldActual;
                }

                if($quantityChange == 0)continue;
                $productQuantityChange = floatval($quantityChange) * -1;//inverse
                $product->quantity += $productQuantityChange;
                if($product->quantity < 0){
                    array_push($errors,"Nicht genügend Lagerbestand (". ($product->quantity * -1) . "x) für Produkt: " . $product->title);
                    $productIdsAndPivotValues[$product->id] = [];
                } else {
                    $product->save();
                    $transaction = new Transaction([
                        "product_id" => $product->id,
                        "user_id" => Auth::user()->id,
                        "description" => trans("transactions.descriptions.service_box"),
                        "quantity" =>  $productQuantityChange,
                    ]);
                    $transaction->save();
                }
            }
        }

        $input = $request->only(['box_template_id','user_id']);
        $box->update($input);

        $box->products()->sync($productIdsAndPivotValues);

        if(count($errors) != 0){
            return Redirect::back()->withErrors($errors);
        }
        if(Auth::user()->can('admin')){
            return redirect()->route('boxes.index');
        } else {
            return redirect()->route('boxes.show',$box->id);
        }
    }

    public function destroy(Box $box)
    {
        return Redirect::back()->withErrors(["not_supported" => 'Service-Boxen können nicht entfernt werden.']);
        /*$box->products()->detach();
        $box->delete();
        return redirect()->route('boxes.index');*/
    }
}
