<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::all();
        return view('orders.index', compact('orders'));
    }

    public function create(Request $request)
    {
        $customers = Customer::all()->pluck('name','id');//custom attribute
        $customerModel = null;
        if(!empty($request->input('cid'))){
            $customerModel = Customer::find($request->input('cid'));
        }
        return view('orders.create',compact('customers','customerModel'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'         => 'required',
            'customer_id'         => 'required|integer',
        ]);

        $input = $request->all();

        $record = Order::create($input);

        return redirect()->route('orders.index');
    }

    public function show(Request $request, Order $order)
    {
        //get missing products from service box
        $productsOrder = $order->products;
        $box = Auth::user()->box;
        $productsBox = $box ? $box->products->keyBy('id') : null;
        $productsRemaining = [];
        $errors = [];

        if(!empty($productsBox)){
            foreach($productsOrder as $product){
                $target = $product->pivot->target;
                $actual = $product->pivot->actual;
                if(isset($target) && isset($actual) && $target >  $actual ){
                    $remainingQuantityOrder = $target - $actual;
                    if(isset($productsBox[$product->id])){
                        $remainingQuantityBox = $remainingQuantityOrder - $productsBox[$product->id]->pivot->actual;
                    } else {
                        $remainingQuantityBox = $remainingQuantityOrder;
                    }

                    if($remainingQuantityBox > 0){
                        $addMissingProductsToBox = $request->input('add_missing_products_to_box');
                        if(isset($addMissingProductsToBox) && $addMissingProductsToBox === "1"){
                            $product->quantity -= $remainingQuantityBox;
                            if($product->quantity < 0){
                                array_push($errors,"Nicht genügend Lagerbestand (". ($product->quantity * -1) . "x) für Produkt: " . $product->title);
                            } else {
                                $productBox = $box->products()->where('box_product.product_id',$product->id)->first();
                                if(isset($productBox)){
                                    $box->products()->syncWithoutDetaching([$productBox->id => [
                                        "actual" => ($productBox->pivot->actual + $remainingQuantityBox),
                                        "target" => $productBox->pivot->target,
                                    ]]);
                                } else {
                                    $box->products()->syncWithoutDetaching([$product->id => [
                                        "actual" => $remainingQuantityBox
                                    ]]);
                                }
                                $product->save();
                            }
                        } else {
                            $productsRemaining[$product->title] = $remainingQuantityBox;
                        }
                    }
                }
            }
        }

        if(count($errors) != 0){
            return Redirect::back()->withErrors($errors);
        }
        return view('orders.show',compact('order','productsRemaining'));
    }

    public function edit(Order $order)
    {
        $customers = Customer::all()->pluck('name','id');//custom attribute
        $products = Product::all()->pluck('title', 'id');
        $productsNumberToId = json_encode(Product::all()->pluck('id', 'number'));
        return view('orders.edit',compact('order','customers','products','productsNumberToId'));
    }

    public function update(Request $request, Order $order)
    {
        $this->validate($request, [
            'title'         => 'required',
            'customer_id'         => 'required|integer',
            'products' => 'array',
            'actuals' => 'array',
            'targets' => 'array',
            'update_inventory' => 'integer|digits:1',
        ]);

        $productIdsAndPivotValues = [];
        $productIds = $request->input('products',[]);
        $actuals = $request->input('actuals',[]);
        $targets = $request->input('targets',[]);

        $errors = [];

        foreach($productIds as $productId){
            $productIdsAndPivotValues[$productId] = ['actual' => $actuals[$productId], 'target' => $targets[$productId]];
        }

         //update products stocks and create transactions
         if($request->input('update_inventory') === "1"){
            foreach($actuals as $productId => $actual){
                $productOrder = $order->products()->where('order_product.product_id',$productId)->first();
                $product = Product::find($productId);
                if(empty($productOrder)){//new product
                    $quantityChange = $actual;
                } else {
                    $oldActual = $productOrder->pivot->actual;
                    $quantityChange = floatval($actual) - $oldActual;
                }

                if($quantityChange == 0)continue;
                $boxQuantityChange = floatval($quantityChange) * -1;//inverse

                $box = Auth::user()->box;
                $updateProductQuantity = true;
                if(isset($box)){
                    $productBox = $box->products()->where('box_product.product_id',$productId)->first();
                    if(isset($productBox)){
                        $remainingBoxQuantity = floatval($productBox->pivot->actual) + $boxQuantityChange;
                        if($remainingBoxQuantity >= 0){
                            $box->products()->updateExistingPivot($productBox->id, ["actual" => $remainingBoxQuantity]);
                            $updateProductQuantity = false;
                        } else {
                            $productQuantityChange = $remainingBoxQuantity;
                            $productsRemaining = $product->quantity + $productQuantityChange;
                            if($productsRemaining < 0){
                                array_push($errors,"Nicht genügend Lagerbestand (". ($productsRemaining * -1) . "x) für Produkt: " . $product->title);
                                $productIdsAndPivotValues[$product->id] = [];
                                $updateProductQuantity = false;
                            } else {
                                $box->products()->updateExistingPivot($productBox->id, ["actual" => 0]);
                            }

                        }
                    }
                }

                if($updateProductQuantity === true){
                    if(!isset($productQuantityChange)){
                        $productQuantityChange = $boxQuantityChange;
                    }
                    $product->quantity += $productQuantityChange;
                    if($product->quantity < 0){
                        array_push($errors,"Nicht genügend Lagerbestand (". ($product->quantity * -1) . "x) für Produkt: " . $product->title);
                    } else {
                        $product->save();
                        $transaction = new Transaction([
                            "product_id" => $product->id,
                            "user_id" => Auth::user()->id,
                            "description" => trans("transactions.descriptions.order") . ' (' . ($order->order_nr ?? $order->title) . ')',
                            "quantity" =>  $productQuantityChange,
                        ]);
                        $transaction->save();
                    }
                }
            }
         }

        $input = $request->only(['title','customer_id','order_nr']);
        $order->update($input);

        $order->products()->sync($productIdsAndPivotValues);

        if(count($errors) != 0){
            return Redirect::back()->withErrors($errors);
        }
        return redirect()->route('orders.index');
    }

    public function destroy(Order $order)
    {
        if($order->products()->count() !== 0){
            return Redirect::back()->withErrors(["relations" => 'Es existieren noch Produkte im Auftrag.']);
        }
        $order->delete();
        return redirect()->route('orders.index');
    }
}
