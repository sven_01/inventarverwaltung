<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $customers = Customer::all();
        return view('customers.index', compact('customers'));
    }

    public function create(Request $request)
    {
        return view('customers.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'company'         => 'required_without:last_name',
            'last_name'         => 'required_without:company',
        ]);

        $input = $request->all();

        $record = Customer::create($input);

        return redirect()->route('customers.index');
    }

    public function show(Customer $customer)
    {
        return view('customers.show',compact('customer'));
    }

    public function edit(Customer $customer)
    {
        return view('customers.edit',compact('customer'));
    }

    public function update(Request $request, Customer $customer)
    {
        $this->validate($request, [
            'company' => 'required',
        ]);

        $input = $request->all();
        $customer->update($input);

        return redirect()->route('customers.index');
    }

    public function destroy(Customer $customer)
    {
        if($customer->orders()->count() !== 0){
            return Redirect::back()->withErrors(["relations" => 'Es existieren noch Fremdbeziehungen.']);
        }
        $customer->delete();
        return redirect()->route('customers.index');
    }
}
