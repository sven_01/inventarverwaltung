<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\BoxTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class BoxTemplateController extends Controller
{
    public function index(Request $request)
    {
        $boxTemplates = BoxTemplate::all();
        return view('boxTemplates.index', compact('boxTemplates'));
    }

    public function create(Request $request)
    {
        $products = Product::all()->pluck('title', 'id');
        return view('boxTemplates.create',compact('products'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'         => 'required',
            'products'         => 'required|array',
            'targets' => 'required|array',
        ]);

        $input = $request->except(['products','targets']);

        $record = BoxTemplate::create($input);

        $products = $request->input('products',[]);
        $targets = $request->input('targets',[]);
        $productIdsAndPivotValues = [];
        foreach($products as $index => $productId){
            $productIdsAndPivotValues[$productId] = ['target' => $targets[$productId]];
        }
        $record->products()->sync($productIdsAndPivotValues);

        return redirect()->route('boxTemplates.index');
    }

    public function show(BoxTemplate $boxTemplate)
    {
        return view('boxTemplates.show',compact('boxTemplate'));
    }

    public function edit(BoxTemplate $boxTemplate)
    {
        $products = Product::all()->pluck('title', 'id');
        $productsNumberToId = json_encode(Product::all()->pluck('id', 'number'));
        return view('boxTemplates.edit',compact('boxTemplate','products','productsNumberToId'));
    }

    public function update(Request $request, BoxTemplate $boxTemplate)
    {
        $this->validate($request, [
            'title' => 'required',
            'products' => 'nullable|array',
            'targets' => 'nullable|array',
        ]);

        $input = $request->except(['products','targets']);
        $boxTemplate->update($input);

        //sync the products
        $products = $request->input('products',[]);
        $targets = $request->input('targets',[]);
        $productIdsAndPivotValues = [];
        foreach($products as $productId){
            $productIdsAndPivotValues[$productId] = ['target' => $targets[$productId]];
        }
        $boxTemplate->products()->sync($productIdsAndPivotValues);

        $boxes = $boxTemplate->boxes;
        foreach($boxes as $box){
            $box->products()->syncWithoutDetaching($productIdsAndPivotValues);
        }

        return redirect()->route('boxTemplates.index');
    }

    public function destroy(BoxTemplate $boxTemplate)
    {
        if($boxTemplate->boxes()->count() !== 0){
            return Redirect::back()->withErrors(["relations" => 'Es existieren noch Fremdbeziehungen.']);
        }
        $boxTemplate->products()->detach();
        $boxTemplate->delete();
        return redirect()->route('boxTemplates.index');
    }
}
