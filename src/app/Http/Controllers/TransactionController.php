<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        $transactions = Transaction::with(['product','user'])->orderBy("created_at","desc")->get();
        return view('transactions.index',compact($transactions));
    }
}
