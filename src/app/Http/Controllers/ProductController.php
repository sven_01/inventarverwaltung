<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::all();
        return view('products.index', compact('products'));
    }

    public function create(Request $request)
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'         => 'required',
            'quantity'         => 'required|integer',
        ]);
        Product::create($request->all());

        return redirect()->route('products.index');
    }

    public function show(Product $product)
    {
        $sortedTransactions = $product->transactions()->orderBy("created_at","desc")->get();
        $product->transactions = $sortedTransactions;
        return view('products.show',compact('product'));
    }

    public function edit(Product $product)
    {
        return view('products.edit',compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'title' => 'required',
            'quantity' => 'required|integer',
            'quantity_alert' => 'nullable|integer',
        ]);
        $input = $request->except(["quantity_add","quantity_remove","description_transaction"]);
        $transaction = new Transaction([
            "product_id" => $product->id,
            "user_id" => Auth::user()->id,
            "description" => $request->input("description_transaction") ?? '',
        ]);
        if(!empty($request->input("quantity_add"))){
            $input["quantity"] += $request->input("quantity_add");
            $transaction["quantity"] = $request->input("quantity_add");
        } else if(!empty($request->input("quantity_remove"))){
            $input["quantity"] -= $request->input("quantity_remove");
            $transaction["quantity"] = "-".$request->input("quantity_remove");
            if($input["quantity"] < 0) {
                $transaction["quantity"] += ($input["quantity"] * -1);
                $input["quantity"] = 0;
            }
        } else { //fixed new quantity
            $transaction["quantity"] +=  $input["quantity"] - $product->quantity;
        }
        $product->update($input);
        if($transaction->quantity != 0){
            $transaction->save();
        }

        return redirect()->route('products.index');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index');
    }
}
