<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        $roles = Role::with('permissions')->get();
        return view('roles.index',["roles" => $roles]);
    }

    public function create(Request $request)
    {
        $permissions = Permission::all()->pluck('name', 'id');
        return view('roles.create', compact('permissions'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'         => 'required',
            'permissions.*' => 'integer',
            'permissions'   => 'required|array',
        ]);
        $role = Role::create($request->all());
        $role->permissions()->sync($request->input('permissions', []));

        return redirect()->route('roles.index');
    }

    public function show(Role $role)
    {
        return view('roles.show',compact('role'));
    }

    public function edit(Role $role)
    {
        $permissions = Permission::all()->pluck('name', 'id');
        return view('roles.edit',compact(['role','permissions']));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'         => 'required',
            'permissions.*' => 'integer',
            'permissions'   => 'required|array',
        ]);
        $role = Role::find($id);
        $role->update($request->all());
        $role->permissions()->sync($request->input('permissions', []));

        return redirect()->route('roles.index');
    }

    public function destroy(Role $role)
    {
        $role->delete();
        return redirect()->route('roles.index');
    }
}
