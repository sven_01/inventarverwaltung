<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::where('name', 'Admin')->first();
        $roleUser = Role::where('name', 'User')->first();

        //Admin
        $permission = Permission::where('name','admin')->firstOr(function () {
            return Permission::create([
                'name' => 'admin',
            ]);
        });
        $permission->assignRole($roleAdmin);

        //User
        $permission = Permission::where('name','user')->firstOr(function () {
            return Permission::create([
                'name' => 'user',
            ]);
        });
        $permission->assignRole($roleUser);

        //User management
        $permission = Permission::where('name','user_management')->firstOr(function () {
            return Permission::create([
                'name' => 'user_management',
            ]);
        });
        $permission->assignRole($roleAdmin);

        //Users
        $permission = Permission::where('name','view users')->firstOr(function () {
            return Permission::create([
                'name' => 'view users',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','create users')->firstOr(function () {
            return Permission::create([
                'name' => 'create users',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','edit users')->firstOr(function () {
            return Permission::create([
                'name' => 'edit users',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','delete users')->firstOr(function () {
            return Permission::create([
                'name' => 'delete users',
            ]);
        });
        $permission->assignRole($roleAdmin);

        //Roles
        $permission = Permission::where('name','view roles')->firstOr(function () {
            return Permission::create([
                'name' => 'view roles',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','create roles')->firstOr(function () {
            return Permission::create([
                'name' => 'create roles',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','edit roles')->firstOr(function () {
            return Permission::create([
                'name' => 'edit roles',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','delete roles')->firstOr(function () {
            return Permission::create([
                'name' => 'delete roles',
            ]);
        });
        $permission->assignRole($roleAdmin);

         //Products
         $permission = Permission::where('name','view products')->firstOr(function () {
            return Permission::create([
                'name' => 'view products',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','create products')->firstOr(function () {
            return Permission::create([
                'name' => 'create products',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','edit products')->firstOr(function () {
            return Permission::create([
                'name' => 'edit products',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','delete products')->firstOr(function () {
            return Permission::create([
                'name' => 'delete products',
            ]);
        });
        $permission->assignRole($roleAdmin);

         //BoxTemplates
         $permission = Permission::where('name','view boxTemplates')->firstOr(function () {
            return Permission::create([
                'name' => 'view boxTemplates',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','create boxTemplates')->firstOr(function () {
            return Permission::create([
                'name' => 'create boxTemplates',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','edit boxTemplates')->firstOr(function () {
            return Permission::create([
                'name' => 'edit boxTemplates',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','delete boxTemplates')->firstOr(function () {
            return Permission::create([
                'name' => 'delete boxTemplates',
            ]);
        });
        $permission->assignRole($roleAdmin);

        //Boxes
        $permission = Permission::where('name','view boxes')->firstOr(function () {
            return Permission::create([
                'name' => 'view boxes',
            ]);
        });
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleUser);

        $permission = Permission::where('name','create boxes')->firstOr(function () {
            return Permission::create([
                'name' => 'create boxes',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','edit boxes')->firstOr(function () {
            return Permission::create([
                'name' => 'edit boxes',
            ]);
        });
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleUser);

        $permission = Permission::where('name','delete boxes')->firstOr(function () {
            return Permission::create([
                'name' => 'delete boxes',
            ]);
        });
        $permission->assignRole($roleAdmin);

        //Customers
        $permission = Permission::where('name','view customers')->firstOr(function () {
        return Permission::create([
            'name' => 'view customers',
        ]);
        });
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleUser);

        $permission = Permission::where('name','create customers')->firstOr(function () {
            return Permission::create([
                'name' => 'create customers',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','edit customers')->firstOr(function () {
            return Permission::create([
                'name' => 'edit customers',
            ]);
        });
        $permission->assignRole($roleAdmin);

        $permission = Permission::where('name','delete customers')->firstOr(function () {
            return Permission::create([
                'name' => 'delete customers',
            ]);
        });
        $permission->assignRole($roleAdmin);

         //Orders
         $permission = Permission::where('name','view orders')->firstOr(function () {
        return Permission::create([
            'name' => 'view orders',
        ]);
        });
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleUser);

        $permission = Permission::where('name','create orders')->firstOr(function () {
            return Permission::create([
                'name' => 'create orders',
            ]);
        });
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleUser);

        $permission = Permission::where('name','edit orders')->firstOr(function () {
            return Permission::create([
                'name' => 'edit orders',
            ]);
        });
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleUser);

        $permission = Permission::where('name','delete orders')->firstOr(function () {
            return Permission::create([
                'name' => 'delete orders',
            ]);
        });
        $permission->assignRole($roleAdmin);
    }
}
