<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name','Admin')->firstOr(function () {
            Role::create([
                'name' => 'Admin',
            ]);
        });

        $role = Role::where('name','User')->firstOr(function () {
            Role::create([
                'name' => 'User',
            ]);
        });
    }
}
