<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultValuesToBoxProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('box_product', function (Blueprint $table) {
            $table->integer("target")->default(0)->nullable()->change();
            $table->integer("actual")->default(0)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('box_product', function (Blueprint $table) {
            $table->integer("target")->nullable()->change();
            $table->integer("actual")->nullable()->change();
        });
    }
}
