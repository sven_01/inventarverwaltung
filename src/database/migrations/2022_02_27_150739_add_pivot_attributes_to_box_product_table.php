<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPivotAttributesToBoxProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('box_product', function (Blueprint $table) {
            $table->integer("target")->nullable();
            $table->integer("actual")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('box_product', function (Blueprint $table) {
            $table->dropColumn("actual");
            $table->dropColumn("target");
        });
    }
}
