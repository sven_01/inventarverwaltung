<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeOrderProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_product', function (Blueprint $table) {
            $table->dropColumn("delivery_status");
            $table->dropColumn("quantity");
            $table->integer("target")->nullable();
            $table->integer("actual")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_product', function (Blueprint $table) {
            $table->dropColumn("actual");
            $table->dropColumn("target");
            $table->string("delivery_status")->nullable();
            $table->integer("quantity")->nullable();
        });
    }
}
